﻿
#include <iostream>
#include<string>

class Play
{
private:
	std::string Player;
	int points;
public:
	Play() : Player("comonName"), points(0)
	{}
	Play(std::string _Player, int _points) : Player(_Player), points(_points)
	{}
	void setPlayerName()
	{
		std::cin >> Player;

	}
	void setPlayerScore()
	{
		std::cin >> points;

	}
	int getPlayerScoreInt()
	{
		return points;
	}
	void getPlayerName()
	{
		std::cout << Player;
	}
	void getPlayerScore()
	{
		std::cout << points;

	}
};

int main()
{
	std::cout << "How many players do you want to add? ";
	int coutOfPlayers;
	int* p = &coutOfPlayers;
	std::cin >> *p;

	Play* players = new Play[*p];

	for (int i = 0; i < coutOfPlayers; i++)
	{
		std::cout << "Enter player " << i+1 << " name: ";
		players[i].setPlayerName();
		std::cout << "\n";
		std::cout << "Enter player " << i + 1 << " score: ";
		players[i].setPlayerScore();
		std::cout << "\n";
	}
	std::cout << "\n";
	for (int i = 0; i < coutOfPlayers - 1; i++) 
	{
		for (int j = i + 1; j < coutOfPlayers; j++) 
		{
			if (players[i].getPlayerScoreInt() < players[j].getPlayerScoreInt())
			{
				Play temp = players[i];
				players[i] = players[j];
				players[j] = temp;
			}
		}
	}
	std::cout << "Sorted player data:"<<"\n";

	for (int i = 0; i < coutOfPlayers; i++)
	{
		std::cout << "Player " << i+1 << " name: ";
		players[i].getPlayerName();
		std::cout << " ";
		std::cout << "'s score = ";
		players[i].getPlayerScore();
		std::cout << "\n";
	}
	players = nullptr;
}

