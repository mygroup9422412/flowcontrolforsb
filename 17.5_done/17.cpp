﻿

#include <iostream>
#include <string.h>

class Vector 
{
public:
    Vector() : x(0), y(0), z(0)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    void SetUserVector()
    {
    std::cout << "add x ";
    std::cin >> x;

    std::cout << "add y ";
    std::cin >> y;

    std::cout << "add z ";
    std::cin >> z;
    }
    void Show()
    {
        std::cout << x<<' ' << y<< ' ' << z<<' ';
    }
    double lengthOfVector()
    {
        double a = sqrt(x * x + y * y + z * z);
        return a;
    }
private:
    double x;
    double y;
    double z;
};

int main()
{
    Vector myVector;
    myVector.SetUserVector();
    myVector.Show();
    std::cout<< "\n"<<"Length of my vector: "<< myVector.lengthOfVector();
}

