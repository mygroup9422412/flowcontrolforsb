﻿
#include <iostream>
#include <string>
class Animal
{
protected:
	std::string voice;
	int paws;

public:
	Animal() :  paws(0), voice("nothing")
	{}
	Animal(int _paws, std::string _voice) : voice(_voice), paws(_paws)
	{}
	int getPawsInt()
	{
		return paws;
	}
	void getPaws()
	{
		std::cout << "this animal have " << paws << "paws " << "\n";
	}
	virtual std::string getVoice()
	{
		return voice;
	}

};
class Dog:public Animal
{
private:

public:
	Dog() :Animal(4, "Woof")
	{}
	std::string getVoice() override
	{
		return voice;
	}
};

class Cat :public Animal
{

public:
	Cat() :Animal(4, "mew")
	{}
	std::string getVoice() override
	{
		return voice;
	}
};

class Octopus :public Animal
{

public:
	Octopus() :Animal(8,"sound of 8 paws")
	{}
	std::string getVoice() override
	{
		return voice;
	}
};
class Fish :public Animal
{

public:
	Fish() :Animal()
	{}
	std::string getVoice() override
	{
		return voice;
	}
};

int main()
{
	int howManyPawsInArrow = 0;
	int coutOfDogs;
	int* a = &coutOfDogs;
	std::cout << "How many dogs, do you whant?" << "\n";
	std::cin >> *a;
	Animal* dogs = new Dog[*a];


	int coutOfCats;
	int* b = &coutOfCats;
	std::cout << "How many cats, do you whant?" << "\n";
	std::cin >> *b;
	Animal* cats = new Cat[*b];

	int coutOfOctopuses;
	int* c = &coutOfOctopuses;
	std::cout << "How many octopuses, do you whant?" << "\n";
	std::cin >> *c;
	Animal* octopuses = new Octopus[*c];

	int coutOfFishes;
	int* d = &coutOfFishes;
	std::cout << "How many fishes, do you whant?" << "\n";
	std::cin >> *d;
	Animal* fishes = new Fish[*d];


	Animal* animals = new Animal[*a + *b + *c + *d];
	for (int i = 0; i < coutOfDogs; i++)
	{
		animals[i] = dogs[i];
	}
	for (int i = 0; i < coutOfCats; i++)
	{
		animals[coutOfDogs + i] = cats[i];
	}
	for (int i = 0; i < coutOfOctopuses; i++)
	{
		animals[coutOfDogs + coutOfCats + i] = octopuses[i];
	}
	for (int i = 0; i < coutOfFishes; i++)
	{
		animals[coutOfDogs + coutOfCats + coutOfOctopuses + i] = fishes[i];
	}
	int coutOfAnimals = coutOfDogs + coutOfCats + coutOfOctopuses + coutOfFishes;
	for (int i = 0; i < coutOfAnimals; i++)
	{

		std::cout<< animals[i].getVoice() << "\n";

	}

	for (int i = 0; i < coutOfAnimals; i++) 
	{

		howManyPawsInArrow += animals[i].getPawsInt();
	}



	std::cout << "\n" << "Paws in arrow: " << howManyPawsInArrow;
	dogs = nullptr;
	cats = nullptr;
	octopuses = nullptr;
	fishes = nullptr;
	animals = nullptr;
}

